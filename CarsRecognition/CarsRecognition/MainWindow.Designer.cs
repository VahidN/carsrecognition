﻿namespace CarsRecognition
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStripAppMenu = new System.Windows.Forms.MenuStrip();
            this.rozpoznawanieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadObrazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recognizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carsPatternsMarekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.skalaSzarościToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wykrywanieBrzegówToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instructionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonLoad = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonProcess = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonCarPatterns = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabelNameDetected = new System.Windows.Forms.ToolStripLabel();
            this.panelForPicture = new System.Windows.Forms.Panel();
            this.pictureBoxCarImage = new System.Windows.Forms.PictureBox();
            this.openFileDialogForCarImage = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonDatabase = new System.Windows.Forms.Button();
            this.buttonRecognize = new System.Windows.Forms.Button();
            this.menuStripAppMenu.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panelForPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCarImage)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripAppMenu
            // 
            this.menuStripAppMenu.BackColor = System.Drawing.Color.Lavender;
            this.menuStripAppMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rozpoznawanieToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStripAppMenu.Location = new System.Drawing.Point(0, 0);
            this.menuStripAppMenu.Name = "menuStripAppMenu";
            this.menuStripAppMenu.Padding = new System.Windows.Forms.Padding(0);
            this.menuStripAppMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStripAppMenu.Size = new System.Drawing.Size(1264, 24);
            this.menuStripAppMenu.TabIndex = 0;
            this.menuStripAppMenu.Text = "menuStrip1";
            // 
            // rozpoznawanieToolStripMenuItem
            // 
            this.rozpoznawanieToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadObrazToolStripMenuItem,
            this.recognizeToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.rozpoznawanieToolStripMenuItem.Name = "rozpoznawanieToolStripMenuItem";
            this.rozpoznawanieToolStripMenuItem.Size = new System.Drawing.Size(101, 24);
            this.rozpoznawanieToolStripMenuItem.Text = "&Rozpoznawanie";
            // 
            // loadObrazToolStripMenuItem
            // 
            this.loadObrazToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("loadObrazToolStripMenuItem.Image")));
            this.loadObrazToolStripMenuItem.Name = "loadObrazToolStripMenuItem";
            this.loadObrazToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadObrazToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.loadObrazToolStripMenuItem.Text = "&Wczytaj obraz..";
            this.loadObrazToolStripMenuItem.Click += new System.EventHandler(this.loadObrazToolStripMenuItem_Click);
            // 
            // recognizeToolStripMenuItem
            // 
            this.recognizeToolStripMenuItem.Image = global::CarsRecognition.Properties.Resources.wifi16;
            this.recognizeToolStripMenuItem.Name = "recognizeToolStripMenuItem";
            this.recognizeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.recognizeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.recognizeToolStripMenuItem.Text = "&Rozpoznaj";
            this.recognizeToolStripMenuItem.Click += new System.EventHandler(this.recognizeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::CarsRecognition.Properties.Resources.exit22;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.exitToolStripMenuItem.Text = "&Zakończ";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.koniecToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carsPatternsMarekToolStripMenuItem,
            this.toolStripSeparator6,
            this.toolStripTextBox1,
            this.skalaSzarościToolStripMenuItem,
            this.wykrywanieBrzegówToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.settingsToolStripMenuItem.Text = "&Ustawienia";
            // 
            // carsPatternsMarekToolStripMenuItem
            // 
            this.carsPatternsMarekToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("carsPatternsMarekToolStripMenuItem.Image")));
            this.carsPatternsMarekToolStripMenuItem.Name = "carsPatternsMarekToolStripMenuItem";
            this.carsPatternsMarekToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.carsPatternsMarekToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.carsPatternsMarekToolStripMenuItem.Text = "Wzorce &marek";
            this.carsPatternsMarekToolStripMenuItem.Click += new System.EventHandler(this.carsPatternsMarekToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(191, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 16);
            this.toolStripTextBox1.Text = "Filtry";
            this.toolStripTextBox1.Visible = false;
            // 
            // skalaSzarościToolStripMenuItem
            // 
            this.skalaSzarościToolStripMenuItem.Name = "skalaSzarościToolStripMenuItem";
            this.skalaSzarościToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.skalaSzarościToolStripMenuItem.Text = "Skala szarości";
            this.skalaSzarościToolStripMenuItem.Visible = false;
            this.skalaSzarościToolStripMenuItem.Click += new System.EventHandler(this.skalaSzarościToolStripMenuItem_Click);
            // 
            // wykrywanieBrzegówToolStripMenuItem
            // 
            this.wykrywanieBrzegówToolStripMenuItem.Name = "wykrywanieBrzegówToolStripMenuItem";
            this.wykrywanieBrzegówToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.wykrywanieBrzegówToolStripMenuItem.Text = "Wykrywanie Brzegów";
            this.wykrywanieBrzegówToolStripMenuItem.Visible = false;
            this.wykrywanieBrzegówToolStripMenuItem.Click += new System.EventHandler(this.wykrywanieBrzegówToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instructionToolStripMenuItem,
            this.toolStripSeparator2,
            this.AboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.helpToolStripMenuItem.Text = "&Pomoc";
            // 
            // instructionToolStripMenuItem
            // 
            this.instructionToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("instructionToolStripMenuItem.Image")));
            this.instructionToolStripMenuItem.Name = "instructionToolStripMenuItem";
            this.instructionToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.instructionToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.instructionToolStripMenuItem.Text = "&Instrukcja";
            this.instructionToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(180, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("AboutToolStripMenuItem.Image")));
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.AboutToolStripMenuItem.Text = "&O programie";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.GhostWhite;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.toolStripButtonLoad,
            this.toolStripButtonProcess,
            this.toolStripSeparator4,
            this.toolStripLabel2,
            this.toolStripButtonCarPatterns,
            this.toolStripSeparator5,
            this.toolStripLabel3,
            this.toolStripButtonHelp,
            this.toolStripButtonAbout,
            this.toolStripSeparator7,
            this.toolStripLabel4,
            this.toolStripLabelNameDetected});
            this.toolStrip1.Location = new System.Drawing.Point(898, 158);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(473, 43);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripLabel1.Size = new System.Drawing.Size(60, 40);
            this.toolStripLabel1.Text = "  Obraz:   ";
            // 
            // toolStripButtonLoad
            // 
            this.toolStripButtonLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLoad.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLoad.Image")));
            this.toolStripButtonLoad.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoad.Name = "toolStripButtonLoad";
            this.toolStripButtonLoad.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripButtonLoad.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonLoad.Text = "Wczytaj obraz..";
            this.toolStripButtonLoad.Click += new System.EventHandler(this.toolStripButtonLoad_Click);
            // 
            // toolStripButtonProcess
            // 
            this.toolStripButtonProcess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonProcess.Image = global::CarsRecognition.Properties.Resources.wifi32;
            this.toolStripButtonProcess.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonProcess.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProcess.Name = "toolStripButtonProcess";
            this.toolStripButtonProcess.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripButtonProcess.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonProcess.Text = "Rozpoznaj markę";
            this.toolStripButtonProcess.Click += new System.EventHandler(this.toolStripButtonProcess_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripLabel2.Size = new System.Drawing.Size(80, 40);
            this.toolStripLabel2.Text = "  Ustawienia: ";
            // 
            // toolStripButtonCarPatterns
            // 
            this.toolStripButtonCarPatterns.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCarPatterns.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCarPatterns.Image")));
            this.toolStripButtonCarPatterns.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCarPatterns.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCarPatterns.Name = "toolStripButtonCarPatterns";
            this.toolStripButtonCarPatterns.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripButtonCarPatterns.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonCarPatterns.Text = "Wzorce marek";
            this.toolStripButtonCarPatterns.Click += new System.EventHandler(this.toolStripButtonCarPatterns_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(57, 40);
            this.toolStripLabel3.Text = "  Pomoc: ";
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripButtonHelp.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonHelp.Text = "Instrukcja korzystania";
            // 
            // toolStripButtonAbout
            // 
            this.toolStripButtonAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAbout.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAbout.Image")));
            this.toolStripButtonAbout.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAbout.Name = "toolStripButtonAbout";
            this.toolStripButtonAbout.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripButtonAbout.Size = new System.Drawing.Size(40, 40);
            this.toolStripButtonAbout.Text = "O programie";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 43);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripLabel4.Size = new System.Drawing.Size(50, 40);
            this.toolStripLabel4.Text = "Wynik: ";
            // 
            // toolStripLabelNameDetected
            // 
            this.toolStripLabelNameDetected.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.toolStripLabelNameDetected.ForeColor = System.Drawing.Color.Navy;
            this.toolStripLabelNameDetected.Name = "toolStripLabelNameDetected";
            this.toolStripLabelNameDetected.Size = new System.Drawing.Size(0, 40);
            // 
            // panelForPicture
            // 
            this.panelForPicture.AutoScroll = true;
            this.panelForPicture.BackColor = System.Drawing.Color.Lavender;
            this.panelForPicture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelForPicture.Controls.Add(this.pictureBoxCarImage);
            this.panelForPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForPicture.Location = new System.Drawing.Point(3, 183);
            this.panelForPicture.Name = "panelForPicture";
            this.panelForPicture.Size = new System.Drawing.Size(919, 554);
            this.panelForPicture.TabIndex = 2;
            // 
            // pictureBoxCarImage
            // 
            this.pictureBoxCarImage.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxCarImage.Name = "pictureBoxCarImage";
            this.pictureBoxCarImage.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxCarImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxCarImage.TabIndex = 0;
            this.pictureBoxCarImage.TabStop = false;
            this.pictureBoxCarImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxCarImage_Paint);
            // 
            // openFileDialogForCarImage
            // 
            this.openFileDialogForCarImage.FileName = "openFileDialog1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.MidnightBlue;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Lavender;
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(1264, 740);
            this.splitContainer1.SplitterDistance = 335;
            this.splitContainer1.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.listView1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(335, 740);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.Color.MidnightBlue;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView1.ForeColor = System.Drawing.Color.GhostWhite;
            this.listView1.Location = new System.Drawing.Point(10, 60);
            this.listView1.Margin = new System.Windows.Forms.Padding(10);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(315, 670);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Zdjęcia";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(73, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Przykładowe zdjęcia";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelForPicture, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 740);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(919, 174);
            this.panel1.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonExit);
            this.groupBox3.Controls.Add(this.buttonAbout);
            this.groupBox3.Controls.Add(this.buttonOpenFile);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.ForeColor = System.Drawing.Color.GhostWhite;
            this.groupBox3.Location = new System.Drawing.Point(498, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(421, 138);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Inne opcje";
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonExit.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonExit.Image = global::CarsRecognition.Properties.Resources.exit64;
            this.buttonExit.Location = new System.Drawing.Point(285, 26);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(120, 100);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "Zakończ";
            this.buttonExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonAbout
            // 
            this.buttonAbout.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAbout.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonAbout.Image = global::CarsRecognition.Properties.Resources.infoIcon;
            this.buttonAbout.Location = new System.Drawing.Point(151, 26);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(120, 100);
            this.buttonAbout.TabIndex = 2;
            this.buttonAbout.Text = "O programie";
            this.buttonAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonAbout.UseVisualStyleBackColor = false;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonOpenFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOpenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOpenFile.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonOpenFile.Image = global::CarsRecognition.Properties.Resources.folder_image;
            this.buttonOpenFile.Location = new System.Drawing.Point(17, 26);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(120, 100);
            this.buttonOpenFile.TabIndex = 1;
            this.buttonOpenFile.Text = "Wczytaj z dysku";
            this.buttonOpenFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOpenFile.UseVisualStyleBackColor = false;
            this.buttonOpenFile.Click += new System.EventHandler(this.buttonOpenFile_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxResult);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.ForeColor = System.Drawing.Color.GhostWhite;
            this.groupBox2.Location = new System.Drawing.Point(308, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(172, 64);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wynik analizy";
            // 
            // textBoxResult
            // 
            this.textBoxResult.BackColor = System.Drawing.Color.GhostWhite;
            this.textBoxResult.Location = new System.Drawing.Point(15, 28);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(145, 22);
            this.textBoxResult.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonDatabase);
            this.groupBox1.Controls.Add(this.buttonRecognize);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.ForeColor = System.Drawing.Color.GhostWhite;
            this.groupBox1.Location = new System.Drawing.Point(4, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 138);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Najważniejsze funkcje";
            // 
            // buttonDatabase
            // 
            this.buttonDatabase.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonDatabase.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDatabase.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonDatabase.Image = global::CarsRecognition.Properties.Resources.database;
            this.buttonDatabase.Location = new System.Drawing.Point(148, 26);
            this.buttonDatabase.Name = "buttonDatabase";
            this.buttonDatabase.Size = new System.Drawing.Size(120, 100);
            this.buttonDatabase.TabIndex = 2;
            this.buttonDatabase.Text = "Baza wzorców";
            this.buttonDatabase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonDatabase.UseVisualStyleBackColor = false;
            this.buttonDatabase.Click += new System.EventHandler(this.buttonDatabase_Click);
            // 
            // buttonRecognize
            // 
            this.buttonRecognize.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonRecognize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRecognize.Enabled = false;
            this.buttonRecognize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRecognize.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonRecognize.Image = global::CarsRecognition.Properties.Resources.CarGrey;
            this.buttonRecognize.Location = new System.Drawing.Point(14, 26);
            this.buttonRecognize.Name = "buttonRecognize";
            this.buttonRecognize.Size = new System.Drawing.Size(120, 100);
            this.buttonRecognize.TabIndex = 1;
            this.buttonRecognize.Text = "Rozpoznaj";
            this.buttonRecognize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonRecognize.UseVisualStyleBackColor = false;
            this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(1264, 764);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStripAppMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripAppMenu;
            this.Name = "MainWindow";
            this.Text = "System wizualnego rozpoznawania marki samochodu - SIP 2009/2010";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStripAppMenu.ResumeLayout(false);
            this.menuStripAppMenu.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panelForPicture.ResumeLayout(false);
            this.panelForPicture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCarImage)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripAppMenu;
        private System.Windows.Forms.ToolStripMenuItem rozpoznawanieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadObrazToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recognizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoad;
        private System.Windows.Forms.ToolStripButton toolStripButtonProcess;
        private System.Windows.Forms.Panel panelForPicture;
        private System.Windows.Forms.PictureBox pictureBoxCarImage;
        private System.Windows.Forms.OpenFileDialog openFileDialogForCarImage;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carsPatternsMarekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instructionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButtonCarPatterns;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripButton toolStripButtonAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabelNameDetected;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonRecognize;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonDatabase;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem skalaSzarościToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wykrywanieBrzegówToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.Button buttonOpenFile;
        private System.Windows.Forms.Button buttonExit;
    }
}

