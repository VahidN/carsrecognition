﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarsRecognition
{
    public partial class Results : Form
    {
        public Results(String result)
        {
            InitializeComponent();
            labelCarName.Text = result;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
} 
