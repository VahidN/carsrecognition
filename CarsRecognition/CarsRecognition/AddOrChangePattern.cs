﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace CarsRecognition
{
    public partial class AddOrChangePattern : Form
    {
        private WindowMode windowMode;
        private PatternDB repository;
        private Pattern pattern;

        public AddOrChangePattern(WindowMode mode, PatternDB repo, Pattern toEdit)
        {
            InitializeComponent();            
            repository = repo;
            windowMode = mode;
            pattern = new Pattern(repository.GetNextId(), Decimal.ToInt32(numericUpDownTreshold.Value), string.Empty, string.Empty,String.Empty);

            openFileDialogForImage.FileName = string.Empty;
            // dodałem ogólny filtr dla obrazów // charfa
            openFileDialogForImage.Filter = "Obrazy|*.bmp;*.jpg;*.jpeg;*.png;*.tiff|Mapa bitowa|*.bmp|Obraz JPG|*.jpg;*.jpeg|Obraz PNG|*.png|Obraz TIFF|*.tiff|All Files|*.*";



            if (mode == WindowMode.EditMode)
            {
                pattern = toEdit;
                loadPatternImage();
                loadMaskImage();
                numericUpDownTreshold.Value = pattern.Treshold;
                textBoxCarMarkName.Text = pattern.Name;
            }

            validateData();

            textBoxCarMarkName.Focus();
        }


        private void loadMaskImage()
        {
            pictureBoxMask.Image = pattern.MaskBitmap;
        }

        private void loadPatternImage()
        {
            pictureBoxPatternView.Image = pattern.PatternBitmap;
        }

        private void validateData()
        {
            int tresholdValue;

            buttonOk.Enabled = !String.IsNullOrEmpty(pattern.Image)
                               && !String.IsNullOrEmpty(pattern.Name) &&
                               !String.IsNullOrEmpty(pattern.Mask) && //ver2
                               Int32.TryParse(pattern.Treshold.ToString(), out tresholdValue);
        }

        private void buttonOpenPattern_Click(object sender, EventArgs e)
        {
            if (openFileDialogForImage.ShowDialog()==DialogResult.OK)
            {
                pattern.Image = openFileDialogForImage.FileName;
                loadPatternImage();
                //loadMaskImage();  ver 1
                //numericUpDownTreshold.Value = pattern.DefaultTreshold; // ładuje domyślny próg przy otwarciu
                validateData();
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {

            //sprawdzenie, czy w bazie istnieje juz marka o takiej nazwie
            Pattern patternWithTheSameName = repository.getByName(textBoxCarMarkName.Text);


            if ((windowMode == WindowMode.EditMode && patternWithTheSameName != null && patternWithTheSameName.Id != pattern.Id)
                || (windowMode == WindowMode.AddMode && patternWithTheSameName != null))
            {
                //przypadek gdy: w bazie istnieje opel. Ktos klika na edytuj toyote i zmienia nazwe toyoty na opel
                //nie dopuszczamy zeby byly dwa wzorce o tych samych nazwach
                //lub tez, gdy ktos klika na dodaj (opel juz jest) a on znowu wpisze opel - tez nie chcemy

                MessageBox.Show(
                    "Podana nazwa marki samochodu jest juz zapisana w bazie wzorców.\nPodaj inną nazwę lub usuń z bazy wzorce, które chcesz nadpisać",
                    "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                if (repository.Update(pattern))
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    MessageBox.Show("Wystąpiły problemy przy zapisie wzorca do bazy, spróbuj ponownie", "Ostrzeżenie",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void numericUpDownTreshold_ValueChanged(object sender, EventArgs e)
        {
            pattern.Treshold = Decimal.ToInt32(numericUpDownTreshold.Value);
            validateData();
        }

        //private void buttonOpenMask_Click(object sender, EventArgs e)
        //{
        //    if (openFileDialogForMask.ShowDialog() == DialogResult.OK)
        //    {
        //       // pattern.Mask = openFileDialogForMask.FileName;
        //        loadMaskImage();
        //        validateData();
        //    }
        //}

        private void textBoxCarMarkName_TextChanged(object sender, EventArgs e)
        {
            pattern.Name = textBoxCarMarkName.Text;
            validateData();
        }

        private void buttonMask_Click(object sender, EventArgs e)
        {
            if (openFileDialogForImage.ShowDialog() == DialogResult.OK)
            {
                pattern.Mask = openFileDialogForImage.FileName;
               // loadPatternImage();
                loadMaskImage(); 
                numericUpDownTreshold.Value = pattern.DefaultTreshold; // ładuje domyślny próg przy otwarciu
                validateData();
            }
        }
    }
}
