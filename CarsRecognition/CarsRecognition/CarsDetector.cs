﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using AForge.Imaging.Filters;
using AForge;

namespace CarsRecognition
{
    // Klasa z wynikiem rozpoznawania, przekazywana w eventach itp.
    public class RecognitionResult
    {
        public bool Found;
        public string Id;
        public int X;
        public int Y;
        public int Scale;
        public RecognitionResult(bool Found)
        {
            this.Found = Found;
            this.Id = "";
            this.X = 0;
            this.Y = 0;
            this.Scale = 0;
        }
        public RecognitionResult(bool Found, string Id, int X, int Y, int Scale)
        {
            this.Found = Found;
            this.Id = Id;
            this.X = X;
            this.Y = Y;
            this.Scale = Scale;
        }
    }
    // Zwracana w zdarzeniu RecognizeCompleted
    public class RecognizeCompletedEventArgs: EventArgs
    {
        public RecognitionResult result;
        public RecognizeCompletedEventArgs(RecognitionResult result)
        {
            this.result = result;
        }
    }
    // Definicja delegata zdarzenia RecognizeCompleted
    public delegate void RecognizeCompletedEventHandler(Object Sender, RecognizeCompletedEventArgs re);

    // Klasa z postępem rozpoznawania - do zdarzeń
    public class RecognitionProgress
    {
        public String Id;
        public int PatternProgress;
        public int TotalProgress;
        public RecognitionProgress(String Id, int PatternProgress, int TotalProgress)
        {
            this.Id = Id;
            this.PatternProgress = PatternProgress;
            this.TotalProgress = TotalProgress;
        }
    }
    // Zwracana w zdarzeniu RecognizeProgress
    public class RecognizeProgressEventArgs : EventArgs
    {
        public RecognitionProgress result;
        public RecognizeProgressEventArgs(RecognitionProgress result)
        {
            this.result = result;
        }
    }
    // Definicja delegata zdarzenia RecognizeProgress
    public delegate void RecognizeProgressEventHandler(Object Sender, RecognizeProgressEventArgs re);

    public class CarsDetector
    {
        // wyciągnąłem z metody, żeby łatwiej było przekazać do wątku
        private CarImage image;
        private PatternDB patternDB;
        private BackgroundWorker bw;
        int resizeStep = 5;
        int minPatternSize = 30;
        int maxPatternSize = 120;

        int croppedTop;
        int croppedLeft;

        // definicja zdarzenia RecognizeCompleted
        public event RecognizeCompletedEventHandler RecognizeCompleted;

        // opakowanie wywołania RecognizeCompleted
        private void OnComplete(RecognitionResult re)
        {
            if (RecognizeCompleted != null)
                RecognizeCompleted(this, new RecognizeCompletedEventArgs(re));
        }

        // definicja zdarzenia RecognizeProgress
        public event RecognizeProgressEventHandler RecognizeProgress;

        // opakowanie wywołania RecognizeProgress
        private void OnProgress(RecognitionProgress re)
        {
            if (RecognizeProgress != null)
                RecognizeProgress(this, new RecognizeProgressEventArgs(re));
        }

        public CarsDetector()
        {
            image = null;
            patternDB = null;
            bw = new BackgroundWorker(); // klasa ułatwiająca tworzenie wątków roboczych
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true; // to się pewnie zmieni
            // metody obsługi tych zdarzeń są wywoływane w wątku głównym,
            // więc można w nich wykonywać operacje na interfejsie
            // (co będzie robione przez łańcuch wywołań zdarzeń)
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            // zaś obsługa tego zdarzenia jest realizowana w osobnym wątku
            // (nie wolno w nim robić nic z interfejsem!)
            bw.DoWork += new DoWorkEventHandler(_Recognize);
        }

        public bool Recognize(CarImage img, PatternDB patDB)
        {
            if (bw.IsBusy)
                return false;
            image = img;
            patternDB = patDB;
            bw.RunWorkerAsync(); // uruchomienie wątku roboczego
            return true;
        }

        // Anulowanie rozpoznawania
        public bool Cancel()
        {
            if (!bw.IsBusy)
                return false;
            bw.CancelAsync();
            return true;
        }

        // obsługa zdarzeń klasy BackgroundWorker
        // zdarzenie wywoływane w trakcie pracy
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OnProgress((RecognitionProgress)e.UserState);
        }

        // zdarzenie wywoływane po zakończeniu wątku
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled || (e.Error!=null))
                OnComplete(new RecognitionResult(false));
            else
                OnComplete((RecognitionResult)e.Result);
        }

        // metoda prywatna odpalana w swoim wątku // charfa
        private void _Recognize(object sender, DoWorkEventArgs e)
        {
            // poupychałem tu i tam liczniki, żeby sprawdzać gdzie leżymy z wydajnością // charfa
            // ...a potem wykomentowałem, bo ich włączanie i wyłączanie też zajmuje kupę czasu :]
//            Stopwatch total = Stopwatch.StartNew();
//            Stopwatch init = Stopwatch.StartNew();
            Bitmap logoPrototype;
            String carId = "";

            int origImageSize = image.CarBitmap.Width;
            //image.ShrinkImageKeepAspectRatio(1024);
            float workScale = origImageSize / image.CarBitmap.Width;

            croppedTop = image.ApplyCroppingTheTop(30);
            image.ApplyCroppingTheBottom(30);
            croppedLeft = image.ApplyCroppingAroundSymmetryAxis(40);

            image.CarBitmap.Save("Cropped.png");
            
            image.ApplyGrayScaleFilter();
            image.ApplyEdgesFilter();
            image.ApplyBlackAndWhiteFilter();

            image.CarBitmap.Save("Resized.png");

            // zamiast brithestPointValue, będzie w stosunku do Treshold
            // żeby mieć względną trafność analizy
            double Accuracy;
            double maxAccuracy = 0;
            int foundX = 0;
            int foundY = 0;
            int foundScale = 0;

            GeneralisedHoughTransform houghTransform = new GeneralisedHoughTransform(image.CarBitmap,200);

//            init.Stop();
//            Console.WriteLine("Init: {0} ms", init.ElapsedMilliseconds);
            
//            Stopwatch fill = new Stopwatch();

            int patternCount = patternDB.Count();
            int i = 0;

            foreach (Pattern pattern in patternDB)
            {
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    e.Result = new RecognitionResult(false);
                    return;
                }

                logoPrototype = pattern.MaskBitmap;
//                fill.Start();
                Accuracy = houghTransform.FillHoughSpace(logoPrototype, minPatternSize, maxPatternSize, resizeStep, pattern.Treshold);
//                fill.Stop();
                //Accuracy = (double)houghTransform.maxValue / ((double)pattern.Treshold * Math.Pow(((double)(minPatternSize + houghTransform.maxZ * resizeStep) / (double)maxPatternSize), 2.0));
                //Accuracy = (double)houghTransform.maxValue / ((double)pattern.Treshold * (double)(minPatternSize + houghTransform.maxZ * resizeStep) / (double)maxPatternSize);
                Console.WriteLine("najtrafniejszy: " + Accuracy);
                Console.WriteLine("prog: " + pattern.Treshold);
                Console.WriteLine("najtrafniejszy dotad: " + maxAccuracy);
                Console.WriteLine("carId: " + pattern.Id);
                Console.WriteLine("maxZ: " + houghTransform.maxZ);
                
                if ((Accuracy > 1) && (Accuracy > maxAccuracy))
                {
                    Console.Out.WriteLine("aaa");
                    maxAccuracy = Accuracy;
                    foundX = (int) (houghTransform.maxX * 2 * workScale) + croppedLeft;
                    foundY = (int) (houghTransform.maxY * 2 * workScale) + croppedTop;
                    foundScale = minPatternSize + houghTransform.maxZ * resizeStep;
                    carId = pattern.Id;
                }
                houghTransform.dumpHoughSpaceToBitmaps(pattern.Name);
                i++;
                bw.ReportProgress(i / patternCount * 100, new RecognitionProgress(pattern.Id, 100, i / patternCount * 100));
            }

            //Console.WriteLine("Fill: {0} ms.", fill.ElapsedMilliseconds);
            //Console.WriteLine("translatePattern: {0} ms.", houghTransform.transpatt.ElapsedMilliseconds);
            //Console.WriteLine("addToHoughSpace: {0} ms.", houghTransform.addtohough.ElapsedMilliseconds);
            //Console.WriteLine("GetPixel: {0} ms.", houghTransform.getpix.ElapsedMilliseconds);

            Console.WriteLine(carId);

            // zwrócenie wartości następuje przez zmienną e.Result
            if (maxAccuracy > 1)
                e.Result = new RecognitionResult(true, carId, foundX, foundY, foundScale);
            else
                e.Result = new RecognitionResult(false);

//            total.Stop();
//            Console.WriteLine("Total: {0} ms.", total.ElapsedMilliseconds);
        }
    }

    public class GeneralisedHoughTransform
    {
        private int houghSpaceHeight, houghSpaceWidth;
        private int[, ,] houghSpace;

        // tablica bajtów z zawartością obrazka
        // po zaaplikowaniu EdgesFilter obrazek ma format pixela 8bppIndexed
        // w którym każdy pixel jest opisany jednym bajtem
        // (nie wiem czy wartość bajtu przekłada się bezpośrednio na jasność pixela, mam taką nadzieję)
        // charfa
        private byte[] imageBytes;
        private int imageBytesWidth;
        private int imageBytesHeight;
        private int imageBytesStride;

        public double maxValue;
        public int maxX;
        public int maxY;
        public int maxZ;

        public GeneralisedHoughTransform(Bitmap processedImage, int startThreshold)
        {        
            // ładowanie danych obrazka do tablicy bajtów
            BitmapData bmpData = processedImage.LockBits(
                new Rectangle(0, 0, processedImage.Width, processedImage.Height),
                ImageLockMode.ReadOnly,
                processedImage.PixelFormat);
            int num_bytes = bmpData.Stride * bmpData.Height;
            imageBytes = new byte[num_bytes];
            System.Runtime.InteropServices.Marshal.Copy(bmpData.Scan0, imageBytes, 0, num_bytes);
            imageBytesWidth = bmpData.Width;
            imageBytesHeight = bmpData.Height;
            imageBytesStride = bmpData.Stride;
            processedImage.UnlockBits(bmpData);

            houghSpaceWidth = processedImage.Width / 2;
            houghSpaceHeight = processedImage.Height / 2;

        }

        private Bitmap resizePatternBitmap(Bitmap patternBitmap, int newSize)
        {
            ResizeBicubic resizeFilter = new ResizeBicubic(newSize, newSize);
            Bitmap newPatternBitmap = resizeFilter.Apply(patternBitmap);

            // create filter
            LevelsLinear levelsFilter = new LevelsLinear();
            // set ranges
            levelsFilter.InRed = new IntRange(0, 150);
            levelsFilter.InGreen = new IntRange(0, 150);
            levelsFilter.InBlue = new IntRange(0, 150);
            // apply the filter
            levelsFilter.ApplyInPlace(newPatternBitmap);

            // create filter
            Edges edgesSharpenFilter = new Edges();
            // apply the filter
            edgesSharpenFilter.ApplyInPlace(newPatternBitmap);

            newPatternBitmap = Grayscale.CommonAlgorithms.BT709.Apply(newPatternBitmap);

            OtsuThreshold otsuFilter = new OtsuThreshold();
            otsuFilter.ApplyInPlace(newPatternBitmap);

            return newPatternBitmap;
        }

        public double FillHoughSpace(Bitmap patternBitmap, int minPatternSize, int maxPatternSize, int resizeStep, int patternThreshold)
        {            
            maxValue = 1.0f;
            maxX = 0;
            maxY = 0;
            maxZ = 0;
            //double max = 0.0;

            int patTres = patternThreshold;

            ResizeBicubic resizeFilter = new ResizeBicubic(patternBitmap.Width, patternBitmap.Height);
            Bitmap resizedPatternBitmap = patternBitmap;

            int[] patternPoints;

            int houghSpaceOtherDimension = ((maxPatternSize - minPatternSize) / resizeStep) + 1;
            int intensity;

            houghSpace = new int[houghSpaceWidth, houghSpaceHeight, houghSpaceOtherDimension];

            for (int i = 0, patternSize = minPatternSize; i < houghSpaceOtherDimension; i++, patternSize += resizeStep)
            {
                Console.WriteLine("Skala... {0}", i);
                //resizeFilter.NewWidth = patternSize;
                //resizeFilter.NewHeight = patternSize;
                //resizedPatternBitmap = resizeFilter.Apply(patternBitmap);

                // próba zachowania lepszej jakości znaczka:
                if (patternBitmap.Width == patternSize)
                    resizedPatternBitmap = patternBitmap;
                else
                    resizedPatternBitmap = resizePatternBitmap(patternBitmap, patternSize);
                //resizedPatternBitmap.Save("pattern" + resizedPatternBitmap.GetHashCode() + "-" + patternSize + ".png");

                patternPoints = translatePattern(resizedPatternBitmap);

                // odwróciłem kolejność pętli - najpierw wybieramy wiersz
                for (int j = 0; j < imageBytesHeight; j++)
                {
                    for (int k = 0; k < imageBytesWidth; k++)
                    {
                        intensity = imageBytes[j*imageBytesStride + k] >> 4; // zamiast GetPix // charfa
                        if (intensity > 0) // to dopiero przyspieszenie // charfa
                        {
                            addToHoughSpaceField(patternPoints, k, j, i, intensity);
//                            max = (double)maxValue / ((double)patternThreshold * Math.Pow(((double)(patternSize) / (double)maxPatternSize), 2.0));
                        }
                    }
                }

                for (int z = 0; z < houghSpaceOtherDimension; z++)
                {
                    int curPatTres = (int)(patTres * (Math.Sqrt((float)(minPatternSize + z * resizeStep)) / Math.Sqrt((float)maxPatternSize)));
                    for (int x = 0; x < houghSpaceWidth; x++)
                        for (int y = 0; y < houghSpaceHeight; y++)
                            if ((float)houghSpace[x, y, z] / (float)curPatTres > maxValue)
                            {
                                maxValue = (float)houghSpace[x, y, z] / (float)curPatTres;
                                maxX = x;
                                maxY = y;
                                maxZ = z;
                            }
                }
            }

            return maxValue;
        }

        // przepisałem w wersji na tablicę intów + inne optymalizacje // charfa
        private int[] translatePattern(Bitmap patternBitmap)
        { 
            // tworzymy tablicę na tyle ile jest punktów wzorca...
            int[] filledPointList = new int[patternBitmap.Width*patternBitmap.Height*3];
            int pixel;
            int next = 0;

            for (int i = 0; i < patternBitmap.Width; i++)
            {
                for (int j = 0; j < patternBitmap.Height; j++)
                {
                    // najpierw dzielimy, potem sprawdzamy, czy większe od zera (było odwrotnie)
                    // w ten sposób nie mamy w tablicy zerowych punktów // charfa
                    pixel = patternBitmap.GetPixel(i, j).R >> 6;
                    if (pixel > 0)
                    {
                        filledPointList[next] = i;
                        filledPointList[next+1] = j;
                        filledPointList[next+2] = pixel;
                        next += 3;
                    }
                }
            }

            // ... ale wybieramy tylko te które wstawiliśmy
            return filledPointList.Take(next).ToArray();
        }

        // przepisałem w wersji na tablicę intów + inne optymalizacje // charfa
        private void addToHoughSpaceField(int[] patternPoints, int x, int y, int z, int intensity)
        {
            int houghX, houghY;
            int numPoints = patternPoints.Count()/3;

            for (int point = 0; point < numPoints; point+=3)
            {
                houghX = (x - patternPoints[point]) / 2;
                houghY = (y - patternPoints[point+1]) / 2;
                if ((houghX > 0) && (houghY > 0))
                {
                    houghSpace[houghX, houghY, z] += (patternPoints[point+2] * intensity);
                }
            }
        }

        // może się przydać przy debugowaniu
        public void dumpHoughSpaceToBitmaps(string base_name)
        {
            for (int i = 0; i < houghSpace.GetLength(2); i++)
            {
                Bitmap bmp = new Bitmap(houghSpaceWidth, houghSpaceHeight, PixelFormat.Format24bppRgb);
                for (int j=0; j<houghSpaceWidth; j++)
                    for (int k=0; k<houghSpaceHeight; k++)
                    {
//                        if (houghSpace[j,k,i] < maxValue)
                            bmp.SetPixel(j, k, Color.FromArgb(255 * houghSpace[j, k, i] / 25000,
                                                              255 * houghSpace[j, k, i] / 25000,
                                                              255 * houghSpace[j, k, i] / 25000));
//                        else if (houghSpace[j,k,i] == maxValue)
//                            bmp.SetPixel(j, k, Color.FromArgb(0,255,0));
//                        else
//                            bmp.SetPixel(j, k, Color.FromArgb(255,0,0));
                    }
                bmp.Save(base_name + i + ".png");
            }
        }
    }
}
