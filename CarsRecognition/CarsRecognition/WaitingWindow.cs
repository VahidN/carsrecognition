﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CarsRecognition
{
    public partial class WaitingWindow : Form
    {
        private CarsDetector carsDetector;
        private PatternDB patternRepository;
        private CarImageManager carImageManager;
        private List<Pattern> listToCheck=new List<Pattern>();
        private int indexOfCurrentImage;
        private int appeard = 0;

        public bool DetectedSuccessfully { get; private set; }
        public int XDetected { get; private set; }
        public int YDetected { get; private set; }
        public int R { get; private set; }
        public String NameDetected { get; private set; }

        private void UpdateProgress(Object Sender, RecognizeProgressEventArgs resultArg)
        {
            progressBar1.PerformStep();
            indexOfCurrentImage++;
            LoadCurrentPicture();
           
        }

        private void ReportRecognized(object Sender, RecognizeCompletedEventArgs results)
        {
            String result = "Nie rozpoznano";

            DetectedSuccessfully = results.result.Found;

            if (DetectedSuccessfully)
            {
                R = (int)results.result.Scale;
                XDetected = results.result.X;// -R / 2;
                YDetected = results.result.Y;// -R / 2;
                
                NameDetected = patternRepository.getById(results.result.Id).Name;
                result = NameDetected;
            }

            Results window=new Results(result);
            appeard++;
            if (appeard==1) {this.Visible = false; window.ShowDialog();}

            this.Close();
        }

        public WaitingWindow(CarsDetector _carsDetector, CarImageManager _carImageManager, PatternDB _patternRepository)
        {
            InitializeComponent();
            carsDetector = _carsDetector;
            carImageManager = _carImageManager;
            patternRepository = _patternRepository;            
            
            carsDetector.RecognizeProgress += new RecognizeProgressEventHandler(UpdateProgress);
            carsDetector.RecognizeCompleted += new RecognizeCompletedEventHandler(ReportRecognized); 
            progressBar1.Maximum = patternRepository.Count();
            progressBar1.Minimum = 0;
            progressBar1.Step = 1;

            XDetected = 0;
            YDetected = 0;
            DetectedSuccessfully = false;
            NameDetected = string.Empty;

            listToCheck = patternRepository.getAllPatterns();
            indexOfCurrentImage = 0;

            LoadCurrentPicture();

        }

        private void LoadCurrentPicture()
        {
            if (listToCheck.Count > indexOfCurrentImage)
                pictureBox1.Image = listToCheck.ElementAt(indexOfCurrentImage).PatternBitmap;
        }

        private void WaitingWindow_Load(object sender, EventArgs e)
        {
        }

        private void WaitingWindow_Shown(object sender, EventArgs e)
        {
            // robię wątki, rejestruj i słuchaj eventów zamiast brać zwracaną wartość
            if (carsDetector.Recognize(carImageManager.CarImageObject, patternRepository))
            { //MessageBox.Show("Liczy się... w tle...");
            }
            else
                MessageBox.Show("Cierpliwości, jeszcze się liczy!");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
                carsDetector.Cancel();

            DetectedSuccessfully = false;
            NameDetected = string.Empty;
            appeard = 2;
            this.Close();
        }

       
    }
}
