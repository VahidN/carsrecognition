﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarsRecognition
{
    public partial class Splash : Form
    {
        private Timer timer;
        private Random r;
        public Splash()
        {
            InitializeComponent();
            timer=new Timer();
            timer.Interval =2;    // przyspieszyłem splasha, niepotrzebnie tak się wlókł // charfa
            timer.Start();
            timer.Tick += new EventHandler(Timer_Tick);
            r=new Random();
        }


         
        private void Timer_Tick(object sender, EventArgs eArgs)
        {
            if (sender == timer)
            {
                for (int i = 0; i < r.Next(1, 20); i++)
                {  
                    progressBar1.PerformStep();
                    if (progressBar1.Value == progressBar1.Maximum) break;
                }

                if (progressBar1.Value == progressBar1.Maximum)
               {
                   timer.Stop();
                   buttonStart.Enabled = true;
                   
               }
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
