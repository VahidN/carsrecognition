﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarsRecognition
{
    public partial class PatternList : Form
    {
        private PatternDB repository;
        private Pattern selectedPattern;

        public PatternList(PatternDB repo)
        {
            InitializeComponent();
            repository = repo;
            refreshData();

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            AddOrChangePattern widnow = new AddOrChangePattern(WindowMode.AddMode, repository, null);
            
            if (widnow.ShowDialog() == DialogResult.OK)
                refreshData();
        }

        private void listBoxPatterns_SelectedValueChanged(object sender, EventArgs e)
        {
            updateSelection();
        }

        private void updateSelection()
        {
            if (listBoxPatterns.SelectedItem != null)
            {
                selectedPattern = repository.getById(listBoxPatterns.SelectedValue.ToString());
                pictureBoxPatternView.Image = selectedPattern.PatternBitmap;
                pictureBoxMask.Image = selectedPattern.MaskBitmap;
                buttonUpdate.Enabled = true;
                buttonRemove.Enabled = true;
               // labelFileName.Text = selectedPattern.Image;
                //labelMaskFileName.Text = selectedPattern.Mask;
                labelthreshold.Text = selectedPattern.Treshold.ToString();
            }
            else
            {
                pictureBoxMask.Image = null;
                pictureBoxPatternView.Image = null;
               // labelFileName.Text = string.Empty;
               // labelMaskFileName.Text = string.Empty;
                labelthreshold.Text = string.Empty;
                buttonUpdate.Enabled = false;
                buttonRemove.Enabled = false;
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            AddOrChangePattern widnow = new AddOrChangePattern(WindowMode.EditMode, repository, selectedPattern);
            if (widnow.ShowDialog() == DialogResult.OK)
                refreshData();
        }

        private void refreshData()
        {
            patternBindingSource.DataSource = repository.getAllPatterns();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (repository.Remove(selectedPattern))
            {
               refreshData();
            }
            else
            {
                MessageBox.Show("Wystąpiły problemy przy próbie usunięcia wzorca, spróbuj ponownie", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
