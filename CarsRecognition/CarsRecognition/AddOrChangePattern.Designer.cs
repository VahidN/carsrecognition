﻿namespace CarsRecognition
{
    partial class AddOrChangePattern
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddOrChangePattern));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBoxMask = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownTreshold = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxPatternView = new System.Windows.Forms.PictureBox();
            this.buttonOpenPattern = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxCarMarkName = new System.Windows.Forms.TextBox();
            this.openFileDialogForMask = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogForImage = new System.Windows.Forms.OpenFileDialog();
            this.buttonMask = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMask)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTreshold)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPatternView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel2);
            this.groupBox4.Location = new System.Drawing.Point(141, 79);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(111, 126);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Maska";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.pictureBoxMask);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(105, 107);
            this.panel2.TabIndex = 0;
            // 
            // pictureBoxMask
            // 
            this.pictureBoxMask.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxMask.Name = "pictureBoxMask";
            this.pictureBoxMask.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxMask.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMask.TabIndex = 0;
            this.pictureBoxMask.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownTreshold);
            this.groupBox3.Location = new System.Drawing.Point(147, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(105, 52);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Próg tolerancji";
            // 
            // numericUpDownTreshold
            // 
            this.numericUpDownTreshold.BackColor = System.Drawing.Color.White;
            this.numericUpDownTreshold.Location = new System.Drawing.Point(12, 20);
            this.numericUpDownTreshold.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownTreshold.Name = "numericUpDownTreshold";
            this.numericUpDownTreshold.Size = new System.Drawing.Size(58, 20);
            this.numericUpDownTreshold.TabIndex = 5;
            this.numericUpDownTreshold.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownTreshold.ValueChanged += new System.EventHandler(this.numericUpDownTreshold_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Location = new System.Drawing.Point(24, 79);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(111, 126);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wzorzec";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBoxPatternView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(105, 107);
            this.panel1.TabIndex = 0;
            // 
            // pictureBoxPatternView
            // 
            this.pictureBoxPatternView.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxPatternView.Name = "pictureBoxPatternView";
            this.pictureBoxPatternView.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxPatternView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxPatternView.TabIndex = 0;
            this.pictureBoxPatternView.TabStop = false;
            // 
            // buttonOpenPattern
            // 
            this.buttonOpenPattern.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonOpenPattern.Image = global::CarsRecognition.Properties.Resources.Download;
            this.buttonOpenPattern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOpenPattern.Location = new System.Drawing.Point(42, 208);
            this.buttonOpenPattern.Name = "buttonOpenPattern";
            this.buttonOpenPattern.Padding = new System.Windows.Forms.Padding(2);
            this.buttonOpenPattern.Size = new System.Drawing.Size(72, 28);
            this.buttonOpenPattern.TabIndex = 2;
            this.buttonOpenPattern.Text = "Wczytaj";
            this.buttonOpenPattern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonOpenPattern.UseVisualStyleBackColor = false;
            this.buttonOpenPattern.Click += new System.EventHandler(this.buttonOpenPattern_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonOk.Image = global::CarsRecognition.Properties.Resources.button_ok32;
            this.buttonOk.Location = new System.Drawing.Point(277, 28);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Padding = new System.Windows.Forms.Padding(3);
            this.buttonOk.Size = new System.Drawing.Size(70, 64);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Zapisz";
            this.buttonOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Image = global::CarsRecognition.Properties.Resources.cancel16;
            this.buttonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCancel.Location = new System.Drawing.Point(277, 98);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Padding = new System.Windows.Forms.Padding(3);
            this.buttonCancel.Size = new System.Drawing.Size(70, 33);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxCarMarkName);
            this.groupBox1.Location = new System.Drawing.Point(24, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(111, 52);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nazwa marki";
            // 
            // textBoxCarMarkName
            // 
            this.textBoxCarMarkName.BackColor = System.Drawing.Color.White;
            this.textBoxCarMarkName.Location = new System.Drawing.Point(6, 20);
            this.textBoxCarMarkName.Name = "textBoxCarMarkName";
            this.textBoxCarMarkName.Size = new System.Drawing.Size(99, 20);
            this.textBoxCarMarkName.TabIndex = 1;
            this.textBoxCarMarkName.TextChanged += new System.EventHandler(this.textBoxCarMarkName_TextChanged);
            // 
            // openFileDialogForMask
            // 
            this.openFileDialogForMask.FileName = "openFileDialog1";
            // 
            // openFileDialogForImage
            // 
            this.openFileDialogForImage.FileName = "openFileDialog2";
            // 
            // buttonMask
            // 
            this.buttonMask.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonMask.Image = global::CarsRecognition.Properties.Resources.Download;
            this.buttonMask.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonMask.Location = new System.Drawing.Point(159, 208);
            this.buttonMask.Name = "buttonMask";
            this.buttonMask.Padding = new System.Windows.Forms.Padding(2);
            this.buttonMask.Size = new System.Drawing.Size(72, 28);
            this.buttonMask.TabIndex = 13;
            this.buttonMask.Text = "Wczytaj";
            this.buttonMask.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonMask.UseVisualStyleBackColor = false;
            this.buttonMask.Click += new System.EventHandler(this.buttonMask_Click);
            // 
            // AddOrChangePattern
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(359, 244);
            this.Controls.Add(this.buttonMask);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonOpenPattern);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddOrChangePattern";
            this.Text = "Wzorzec marki";
            this.groupBox4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMask)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTreshold)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPatternView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBoxMask;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxPatternView;
        private System.Windows.Forms.Button buttonOpenPattern;
        private System.Windows.Forms.NumericUpDown numericUpDownTreshold;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxCarMarkName;
        private System.Windows.Forms.OpenFileDialog openFileDialogForMask;
        private System.Windows.Forms.OpenFileDialog openFileDialogForImage;
        private System.Windows.Forms.Button buttonMask;
    }
}