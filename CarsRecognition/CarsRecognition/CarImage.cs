﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using AForge.Imaging.Filters;

namespace CarsRecognition
{
    /*public class SymmetryAxis
    {
        public double a, b;
        // x = ay + b
        // a = 1/a1, b = -b1/a1

        public SymmetryAxis()
        {
            a = 0;
            b = 0;
        }

        public SymmetryAxis(int b)
        {
            a = 0;
            this.b = (double)b;
        }
    }*/
    
    public class CarImage
    {
        public String FilePath { get; private set; }
        public bool IsLoaded { get; private set; }
        public Bitmap CarBitmap { get; private set; }
        private Random random = new Random();
        int axisNeighborhoodWidthFactor = 20;

        public bool LoadPicture(string filePath)
        {
            try
            {
                CarBitmap = new Bitmap(Image.FromFile(filePath));
                FilePath = filePath;
                IsLoaded = true;
                return true;
            }
            catch 
            {
                CarBitmap = null;
                FilePath = string.Empty;
                IsLoaded = false;
                return false;
            }
        }

        public bool LoadPicture(Bitmap bitmap)
        {
            try
            {
                CarBitmap = bitmap;
                FilePath = "";
                IsLoaded = true;
                return true;
            }
            catch
            {
                CarBitmap = null;
                FilePath = string.Empty;
                IsLoaded = false;
                return false;
            }
        }

        public void ClosePicure()
        {
            CarBitmap = null;
            FilePath = string.Empty;
            IsLoaded = false;   
        }

        public bool ApplyEdgesFilter()
        {
            if (IsLoaded)
            {
                CannyEdgeDetector edgesFilter = new CannyEdgeDetector(20, 75, 1.5);
                CarBitmap = edgesFilter.Apply(CarBitmap);
                return true;
            }

            return false;
        }

        public bool ResizeImage(int width, int height)
        {
            if (IsLoaded)
            {
                ResizeNearestNeighbor resize = new ResizeNearestNeighbor(width, height);
                CarBitmap = resize.Apply(CarBitmap);
                return true;
            }

            return false;
        }

        public bool ResizeImageKeepAspectRatio(int maxDimension)
        {
            int height, width;
            double aspectRatio;

            if (IsLoaded)
            {
                height = CarBitmap.Height;
                width = CarBitmap.Width;
                aspectRatio = (double)width / (double)height;
                if (width > height)
                {
                    width = maxDimension;
                    height = (int)((double)width / aspectRatio);
                }
                else
                {
                    height = maxDimension;
                    width = (int)((double)height * aspectRatio);
                }

                ResizeBicubic resize = new ResizeBicubic(width, height);
                CarBitmap = resize.Apply(CarBitmap);
                return true;
            }

            return false;
        }

        public bool ShrinkImageKeepAspectRatio(int maxDimension)
        {
            int height, width;
            double aspectRatio;

            if (IsLoaded)
            {
                height = CarBitmap.Height;
                width = CarBitmap.Width;

                if ((width > maxDimension) || (height > maxDimension))
                {
                    aspectRatio = (double)width / (double)height;
                    if (width > height)
                    {
                        width = maxDimension;
                        height = (int)((double)width / aspectRatio);
                    }
                    else
                    {
                        height = maxDimension;
                        width = (int)((double)height * aspectRatio);
                    }

                    ResizeBicubic resize = new ResizeBicubic(width, height);
                    CarBitmap = resize.Apply(CarBitmap);
                    return true;
                }
                return false;
            }

            return false;
        }

        public bool ApplyGrayScaleFilter()
        {
            if (IsLoaded)
            {
                CarBitmap = Grayscale.CommonAlgorithms.BT709.Apply(CarBitmap);
                return true;
            }

            return false;
        }

        public bool ApplyBlackAndWhiteFilter()
        {
            if (IsLoaded)
            {
                //CarBitmap = Grayscale.CommonAlgorithms.BT709.Apply(CarBitmap);

                OtsuThreshold otsuThreshold = new OtsuThreshold();
                otsuThreshold.ApplyInPlace(CarBitmap);

                return true;
            }

            return false;
        }

        public int ApplyCroppingTheTop(int percentToCrop)
        {       
            float fraction = (float)percentToCrop / 100.0f;
            int newStart = (int)((float)CarBitmap.Height * fraction);
            Crop crop = new Crop(new Rectangle(0, newStart, CarBitmap.Width, CarBitmap.Height - newStart));
            CarBitmap = crop.Apply(CarBitmap);

            return newStart;
        }

        public int ApplyCroppingTheBottom(int percentToCrop)
        {
            int oldEnd = CarBitmap.Height;
            float fraction = (100.0f - (float)percentToCrop) / 100.0f;
            int newEnd = (int)((float)CarBitmap.Height * fraction);
            Crop crop = new Crop(new Rectangle(0, 0, CarBitmap.Width, newEnd));
            CarBitmap = crop.Apply(CarBitmap);

            return (oldEnd - newEnd);
        }

        private double evaluateAxis(int b, Bitmap processedImage)
        {
            int x1, x2, y1, y2, numberOfPixels = 1000;
            int minWidth, maxWidth, minHeight, maxHeight;
            int pix1, pix2;//, delta;
            double averageDouble = 0.0;

            minWidth = b - processedImage.Width / axisNeighborhoodWidthFactor;
            if (minWidth < 0) minWidth = 0;
            maxWidth = b + processedImage.Width / axisNeighborhoodWidthFactor;
            if (maxWidth > processedImage.Width) maxWidth = processedImage.Width;
            minHeight = 0;// processedImage.Height * 1 / 3;
            maxHeight = processedImage.Height;// *4 / 5;

            int counter = 0;

            bool[,] alreadyVisited = new bool[maxWidth - minWidth, maxHeight - minHeight];

            for (int i = 0; i < numberOfPixels; i++)
            {
                do
                {
                    x1 = random.Next(minWidth, maxWidth);
                    y1 = random.Next(minHeight, maxHeight);
                }
                while (alreadyVisited[x1 - minWidth, y1 - minHeight] == true);

                alreadyVisited[x1 - minWidth, y1 - minHeight] = true;

                y2 = y1;
                x2 = x1 + (b - x1) * 2;

                if (x2 > 0 && x2 < processedImage.Width)
                {
                    pix1 = processedImage.GetPixel(x1, y1).R;
                    pix2 = processedImage.GetPixel(x2, y2).R;

                    //delta = pix1 - pix2;
                    //if (delta < 0) delta = -delta;

                    //averageDouble += (double)delta;
                    if (pix1 != pix2) averageDouble += 1.0;
                    counter++;
                }
            }

            averageDouble /= (double)counter;

            return averageDouble;
        }

        private double evaluateAxisWithColor(int b, Bitmap processedImage)
        {
            int x1, x2, y1, y2, numberOfPixels = 1000;
            int minWidth, maxWidth, minHeight, maxHeight;

            int deltaR, deltaG, deltaB;
            double deltaR2, deltaG2, deltaB2;
            double averageDouble = 0.0;

            minWidth = b - processedImage.Width / axisNeighborhoodWidthFactor;
            if (minWidth < 0) minWidth = 0;
            maxWidth = b + processedImage.Width / axisNeighborhoodWidthFactor;
            if (maxWidth > processedImage.Width) maxWidth = processedImage.Width;
            minHeight = 0;// processedImage.Height * 1 / 3;
            maxHeight = processedImage.Height;// *4 / 5;

            int counter = 0;

            bool[,] alreadyVisited = new bool[maxWidth - minWidth, maxHeight - minHeight];

            for (int i = 0; i < numberOfPixels; i++)
            {
                do
                {
                    x1 = random.Next(minWidth, maxWidth);
                    y1 = random.Next(minHeight, maxHeight);
                }
                while (alreadyVisited[x1 - minWidth, y1 - minHeight] == true);

                alreadyVisited[x1 - minWidth, y1 - minHeight] = true;

                y2 = y1;
                x2 = x1 + (b - x1) * 2;

                if (x2 > 0 && x2 < processedImage.Width)
                {
                    deltaR = processedImage.GetPixel(x1, y1).R;
                    deltaR -= processedImage.GetPixel(x2, y2).R;
                    deltaR2 = Math.Pow((double)deltaR, 2.0);

                    deltaG = processedImage.GetPixel(x1, y1).G;
                    deltaG -= processedImage.GetPixel(x2, y2).G;
                    deltaG2 = Math.Pow((double)deltaG, 2.0);

                    deltaB = processedImage.GetPixel(x1, y1).B;
                    deltaB -= processedImage.GetPixel(x2, y2).B;
                    deltaB2 = Math.Pow((double)deltaB, 2.0);

                    averageDouble += Math.Sqrt(deltaR2 + deltaG2 + deltaB2);
                    counter++;
                }
            }

            averageDouble /= (double)counter;

            return averageDouble;
        }

        private int findSymmetry(Bitmap processedImage)
        {
            processedImage = Grayscale.CommonAlgorithms.BT709.Apply(processedImage);

            SobelEdgeDetector sobel = new SobelEdgeDetector();
            sobel.ApplyInPlace(processedImage);

            /*CannyEdgeDetector canny = new CannyEdgeDetector(20, 75, 1.5);
            canny.ApplyInPlace(processedCarBitmap);*/

            OtsuThreshold otsu = new OtsuThreshold();
            otsu.ApplyInPlace(processedImage);

            BinaryDilatation3x3 dilatation = new BinaryDilatation3x3();
            //Dilatation3x3 dilatation = new Dilatation3x3();
            //dilatation.ApplyInPlace(processedCarBitmap);
            dilatation.ApplyInPlace(processedImage);
            dilatation.ApplyInPlace(processedImage);
            //dilatation.ApplyInPlace(processedCarBitmap);
            processedImage.Save("afterDilBinary.png");

            int margin = (int)((double)processedImage.Width * 0.05);
            int b = processedImage.Width / 2, bestB = b;
            double eval = evaluateAxis(b, processedImage), bestEval = eval;

            while(b < processedImage.Width - margin)
            {
                eval = evaluateAxis(b, processedImage);

                if (eval < bestEval)
                {
                    bestB = b;
                    bestEval = eval;
                }

                b += 5;
            }

            b = processedImage.Width / 2;

            while (b > margin)
            {
                eval = evaluateAxisWithColor(b, processedImage);

                if (eval < bestEval)
                {
                    bestB = b;
                    bestEval = eval;
                }

                b -= 5;
            }

            return bestB;
        }

        private int findSymmetryWithColor(Bitmap processedImage)
        {
            int margin = (int)((double)processedImage.Width * 0.2);
            int b = processedImage.Width / 2, bestB = b;
            double eval = evaluateAxisWithColor(b, processedImage), bestEval = eval;

            while (b < processedImage.Width - margin)
            {
                eval = evaluateAxisWithColor(b, processedImage);

                if (eval < bestEval)
                {
                    bestB = b;
                    bestEval = eval;
                }

                b += 5;
            }

            b = processedImage.Width / 2;

            while (b > margin)
            {
                eval = evaluateAxisWithColor(b, processedImage);

                if (eval < bestEval)
                {
                    bestB = b;
                    bestEval = eval;
                }

                b -= 5;
            }

            return bestB;
        }

        public void ApplyCroppingBetweenLights()
        { 
            // mam parę pomysłów... Marta
            // ściągnijcie sobie Image Processing Lab ze strony aForge'a
            // i też pokombinujcie jak wykryć obszar między światłami

            // pomysł 1: zrobić pikselozę (32, a potem jeszcze trochę)
            // i sprawdzić czy auto nie jest
            // pomarańczowe, jeśli nie - zrobić mniejszą pikselozę
            // i poszukać pomarańczowych pikseli (kierunkowskazy)
            // w pewnym odstępie w jednej linii,
            // jeśli tak - skorzystać z innych pomysłów
            // pomysł 2: zrobić pikselozę (32) i zmanipulować poziomy
            // (np. ustawiając dolny input na 135, a górny na 195 przy
            // zsynchronizowanych poziomach), poszukać jasnych pikseli 
            // w pewnym odstępie w jednej linii
            // pomysł 3: zrobić pikselozę (32), zmanipulować poziomy,
            // zmniejszyć kontrast do minimum (sic!) i obchlastać
            // jednolite tło, zostawić kolorowe centrum
            // itd...
        }

        public int ApplyCroppingAroundSymmetryAxis(int percentToLeave)
        {
            Bitmap processedCarBitmap = new Bitmap(CarBitmap);

            /*Bitmap processedCarBitmap = CarBitmap.Clone(new Rectangle(0, 0, CarBitmap.Width, CarBitmap.Height), PixelFormat.Format24bppRgb);

            Dilatation3x3 dilatation = new Dilatation3x3();
            dilatation.ApplyInPlace(processedCarBitmap);
            dilatation.ApplyInPlace(processedCarBitmap);
            dilatation.ApplyInPlace(processedCarBitmap);
            processedCarBitmap.Save("afterDilColor.png");
            processedCarBitmap = Grayscale.CommonAlgorithms.BT709.Apply(processedCarBitmap);*/

            //int newStart = (int)((float)processedCarBitmap.Width * 0.1);
            int newWidth;// = (int)((float)processedCarBitmap.Width * 0.8);
            //Crop preCrop = new Crop(new Rectangle(newStart, 0, newWidth, processedCarBitmap.Height));
            //processedCarBitmap = preCrop.Apply(processedCarBitmap);
            processedCarBitmap.Save("afterDil1.png");

            int b = findSymmetryWithColor(processedCarBitmap);

            processedCarBitmap.Save("afterDil2.png");
            
            float fraction = (float)percentToLeave / 100.0f;
            int x = b;
            newWidth = (int)((float)CarBitmap.Width * fraction);
            x -= newWidth / 2;
            
            Crop crop = new Crop(new Rectangle(x, 0, newWidth, CarBitmap.Height));
            CarBitmap = crop.Apply(CarBitmap);

            return x;
        }
    }
}