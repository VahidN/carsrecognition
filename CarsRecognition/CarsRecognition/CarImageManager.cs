﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarsRecognition
{
    public class CarImageManager
    {
        private static CarImage carImageObject;

        public CarImage CarImageObject
        {
            get 
            {
                if (carImageObject == null) carImageObject = new CarImage();
                return carImageObject;
            }
        }
    }
}