﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace CarsRecognition
{
    public partial class MainWindow : Form
    {
        private bool splashOn = true; // zmienna do włączania/wyłączania splashscreena
        // przydaje się do debugowania...
        
        private CarImageManager carImageManager;
        private CarsDetector carsDetector;
        private PatternDB patternRepository;

        private ImageList carsSampleList=new ImageList();

        private int xDetected = 50;
        private int yDetected = 50;
        private bool detected = false;
        private string nameDetected = string.Empty;
        private int r = 50;


        public MainWindow()
        {
            InitializeComponent();
            carImageManager = new CarImageManager();
            carsDetector = new CarsDetector();
            // tutaj dodałem prostą obsługę zdarzenia, trzeba to będzie rozwinąć
            //carsDetector.RecognizeCompleted += new RecognizeCompletedEventHandler(delegate { MessageBox.Show("Policzyło się"); });
            patternRepository = new PatternDB();
            // dodałem StartupPath, żeby była ścieżka bezwzględna - inaczej się kaszaniło wszystko // charfa
            patternRepository.Connect(Application.StartupPath + @"\BazaWzorcow.xml");
            openFileDialogForCarImage.FileName = string.Empty;
            // dodałem ogólny filtr // charfa
            openFileDialogForCarImage.Filter = "Obrazy|*.bmp;*.jpg;*.jpeg;*.png;*.tiff|Mapa bitowa|*.bmp|Obraz JPG|*.jpg;*.jpeg|Obraz PNG|*.png|Obraz TIFF|*.tiff|All Files|*.*";
            carsSampleList.ImageSize=new Size(200,150);

            listView1.LargeImageList = carsSampleList;
            //ResourceManager rm = new ResourceManager("myResources", Assembly.GetExecutingAssembly());
           

            carsSampleList.Images.Add("1",new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audiTT));
            listView1.Items.Add("Audi TT","1");
            carsSampleList.Images.Add("2", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audi_a3_front_24_4_08));
            listView1.Items.Add("Audi A3", "2");
            carsSampleList.Images.Add("3", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audi_front));
            listView1.Items.Add("Audi", "3");
            carsSampleList.Images.Add("4", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7494));
            listView1.Items.Add("Audi", "4");
            carsSampleList.Images.Add("5", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources._431917_1_lg_small));
            listView1.Items.Add("Ford Escape", "5");
            carsSampleList.Images.Add("6", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7647));
            listView1.Items.Add("Ford", "6");
            carsSampleList.Images.Add("7", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7647));
            listView1.Items.Add("Peugeot", "7");
            carsSampleList.Images.Add("8", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7604));
            listView1.Items.Add("Peugeot", "8");
            carsSampleList.Images.Add("9", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7650));
            listView1.Items.Add("Peugeot", "9");
           // carsSampleList.Images.Add("7", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.opel_astra));
           // listView1.Items.Add("Opel Astra", "7");
           // carsSampleList.Images.Add("8", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.Opel_Omega_B_Sportcoupe_MV6));
           // listView1.Items.Add("Opel Omega B", "8");
           // carsSampleList.Images.Add("9", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.opel_omega_c));
           // listView1.Items.Add("Opel Omega C", "9");
           // carsSampleList.Images.Add("10", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7458));
           // listView1.Items.Add("Opel", "10");
          //  carsSampleList.Images.Add("11", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.toyota_corolla_01));
          //  listView1.Items.Add("Toyota Corolla", "11");
           // carsSampleList.Images.Add("12", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.toyota_yaris));
           // listView1.Items.Add("Toyota Yaris", "12");
            //carsSampleList.Images.Add("13", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7651));
            //listView1.Items.Add("Toyota", "13");
            //carsSampleList.Images.Add("14", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.S80_exterior_07));
            //listView1.Items.Add("Volvo S80", "14");
            //carsSampleList.Images.Add("15", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.Volvo_S60_concept_car));
            //listView1.Items.Add("Volvo S60", "15");
            //carsSampleList.Images.Add("16", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7463));
            //listView1.Items.Add("Volvo", "16");
            //carsSampleList.Images.Add("17", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.volvo2_2_));
            //listView1.Items.Add("Volvo", "17");
        }

        private void koniecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButtonLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialogForCarImage.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                carImageManager.CarImageObject.LoadPicture(openFileDialogForCarImage.FileName);
                pictureBoxCarImage.Image = carImageManager.CarImageObject.CarBitmap;
                textBoxResult.Text = string.Empty;
            }
        }

        private void loadObrazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonOpenFile_Click(sender, e);
        }

        private void skalaSzarościToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carImageManager.CarImageObject.ApplyGrayScaleFilter();
            pictureBoxCarImage.Image = carImageManager.CarImageObject.CarBitmap;
        }

        private void wykrywanieBrzegówToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carImageManager.CarImageObject.ApplyEdgesFilter();
            pictureBoxCarImage.Image = carImageManager.CarImageObject.CarBitmap;
        }

        private void toolStripButtonProcess_Click(object sender, EventArgs e)
        {
            if (carImageManager.CarImageObject.IsLoaded == false)
            {
                MessageBox.Show("Nie wczytano zdjęcia do rozpoznania", "Informacja", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            else
            {
                if (patternRepository.Count() == 0) 
                {
                    MessageBox.Show("Baza wzorców marek jest pusta. Proszę uzupełnić baze danych i spróbować ponownie",
                                    "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                WaitingWindow window = new WaitingWindow(carsDetector, carImageManager, patternRepository);
                window.ShowDialog();
                detected = window.DetectedSuccessfully;
                if (detected)
                {
                    xDetected = window.XDetected;
                    yDetected = window.YDetected;
                    nameDetected = window.NameDetected;
                    r = window.R;
                }

                textBoxResult.Text = detected ? nameDetected : "Marka nieznana";
                pictureBoxCarImage.Invalidate();
            }

        }

        private void toolStripButtonCarPatterns_Click(object sender, EventArgs e)
        {
            PatternList patternListWindow = new PatternList(patternRepository);
            patternListWindow.ShowDialog();
  
        }

        private void carsPatternsMarekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonDatabase_Click(sender, e);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            if (splashOn)
            {
                this.Hide();
                Splash splash = new Splash();
                splash.ShowDialog();
                this.Show();
            }
        }

        private void pictureBoxCarImage_Paint(object sender, PaintEventArgs e)
        {
            if (detected == true)
            {
                //e.Graphics.DrawEllipse(new Pen(Color.Lime, 3), xDetected, yDetected, r, r);
                e.Graphics.DrawRectangle(new Pen(Color.Lime, 3), xDetected, yDetected, r, r);
                detected = false;
            }
        }

        private void recognizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonRecognize_Click(sender, e);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            textBoxResult.Text = string.Empty;
            buttonRecognize.Enabled = true;

            carsSampleList.Images.Add("1", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audiTT));
            carsSampleList.Images.Add("2", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audi_a3_front_24_4_08));
            carsSampleList.Images.Add("3", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.audi_front));
            carsSampleList.Images.Add("4", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7494));
            carsSampleList.Images.Add("5", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources._431917_1_lg_small));
            carsSampleList.Images.Add("6", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7647));
            carsSampleList.Images.Add("7", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7496));
            carsSampleList.Images.Add("8", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7604));
            carsSampleList.Images.Add("9", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7650));

            //carsSampleList.Images.Add("7", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.opel_astra));
            //carsSampleList.Images.Add("8", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.Opel_Omega_B_Sportcoupe_MV6));
            //carsSampleList.Images.Add("9", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.opel_omega_c));
            //carsSampleList.Images.Add("10", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7458));
            //carsSampleList.Images.Add("11", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.toyota_corolla_01));
            //carsSampleList.Images.Add("12", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.toyota_yaris));
            //carsSampleList.Images.Add("13", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7651));
            //carsSampleList.Images.Add("14", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.S80_exterior_07));
            //carsSampleList.Images.Add("15", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.Volvo_S60_concept_car));
            //carsSampleList.Images.Add("16", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.IMGP7463));
            //carsSampleList.Images.Add("17", new ResizeNearestNeighbor(200, 150).Apply(Properties.Resources.volvo2_2_));

            switch (listView1.SelectedItems[0].ImageKey)
            {
                case "1":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.audiTT);
                    break;
                case "2":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.audi_a3_front_24_4_08);
                    break;
                case "3":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.audi_front);
                    break;
                case "4":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7494);
                    break;
                case "5":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources._431917_1_lg_small);
                    break;
                case "6":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7647);
                    break;
                case "7":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7496);
                    break;
                case "8":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7604);
                    break;
                case "9":
                    carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7650);
                    break;
                    //    case "7": carImageManager.CarImageObject.LoadPicture(Properties.Resources.opel_astra); break;
                    //    case "8": carImageManager.CarImageObject.LoadPicture(Properties.Resources.Opel_Omega_B_Sportcoupe_MV6); break;
                    //    case "9": carImageManager.CarImageObject.LoadPicture(Properties.Resources.opel_omega_c); break;
                    //    case "10": carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7458); break;
                    //    case "11": carImageManager.CarImageObject.LoadPicture(Properties.Resources.toyota_corolla_01); break;
                    //    case "12": carImageManager.CarImageObject.LoadPicture(Properties.Resources.toyota_yaris); break;
                    //    case "13": carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7651); break;
                    //    case "14": carImageManager.CarImageObject.LoadPicture(Properties.Resources.S80_exterior_07); break;
                    //    case "15": carImageManager.CarImageObject.LoadPicture(Properties.Resources.Volvo_S60_concept_car); break;
                    //    case "16": carImageManager.CarImageObject.LoadPicture(Properties.Resources.IMGP7463); break;
                    //    case "17": carImageManager.CarImageObject.LoadPicture(Properties.Resources.volvo2_2_); break;
                    //}
            }
            pictureBoxCarImage.Image = carImageManager.CarImageObject.CarBitmap;
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialogForCarImage.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                carImageManager.CarImageObject.LoadPicture(openFileDialogForCarImage.FileName);
                pictureBoxCarImage.Image = carImageManager.CarImageObject.CarBitmap;
                textBoxResult.Text = string.Empty;
                buttonRecognize.Enabled = true;
            }
        }

        private void buttonRecognize_Click(object sender, EventArgs e)
        {
            if (carImageManager.CarImageObject.IsLoaded == false)
            {
                MessageBox.Show("Nie wczytano zdjęcia do rozpoznania", "Informacja", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            else
            {
                patternRepository.getAllPatterns();
                if (patternRepository.Count() == 0)
                {
                    MessageBox.Show("Baza wzorców marek jest pusta. Proszę uzupełnić baze danych i spróbować ponownie",
                                    "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                WaitingWindow window = new WaitingWindow(carsDetector, carImageManager, patternRepository);
                window.ShowDialog();
                detected = window.DetectedSuccessfully;
                if (detected)
                {
                    xDetected = window.XDetected;
                    yDetected = window.YDetected;
                    nameDetected = window.NameDetected;
                    r = window.R;
                }

                textBoxResult.Text = detected ? nameDetected : "Marka nieznana";
                pictureBoxCarImage.Invalidate();
            }
        }

        private void buttonDatabase_Click(object sender, EventArgs e)
        {
            PatternList patternListWindow = new PatternList(patternRepository);
            patternListWindow.ShowDialog();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            About window=new About();
            window.ShowDialog();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonAbout_Click(sender,e);
        }
    }
}
