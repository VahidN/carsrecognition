﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using AForge.Imaging.Filters;

namespace CarsRecognition
{
    public class Pattern
    {

        private Bitmap maskBitmap;
        private Bitmap patternBitmap;
        private String image;
        private String mask;   //2 ver
        private int defTreshold;

        public String Id { get; set; }
        public int Treshold { get; set; }

        // wylicza domyślny próg
        public int DefaultTreshold
        {
            get
            {
               // if (defTreshold == 0)  //ver 1
              //  {
                    if (!String.IsNullOrEmpty(Image))
                    {
                        int tmp = 0;
                        for (int i = 0; i < MaskBitmap.Width; i++)
                            for (int j = 0; j < MaskBitmap.Height; j++)
                                tmp += MaskBitmap.GetPixel(i, j).R;
                        defTreshold = (tmp >> 6) * 8 / 10;
                  //  }
                }
                return defTreshold;
            }
        }

        public String Name { get; set; }

        public String Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
                patternBitmap = null; // dzieki temu, w przypadku zarzadania bitmapy, bedzie wygenerowana od nowa
               // maskBitmap = null;  //1 ver
                defTreshold = 0;
            }
        }

        public String Mask   //ver 2
        {
            get
            {
                return mask;
            }

            set
            {
                mask = value;
                maskBitmap = null;
            }
        }

        /// <summary>
        /// Zwraca bitmape 100x100 maski wzorca - krawędzie
        /// <remarks>
        /// Tworzona tylko raz, na żądanie użytkownika
        /// </remarks>
        /// </summary>
        public Bitmap MaskBitmap
        { 
            //get
            //{
            //    //#region 1ver
            //    //if (maskBitmap == null)
            //    //    if (!String.IsNullOrEmpty(Image))
            //    //    {
            //    //        maskBitmap = new ResizeNearestNeighbor(100, 100).Apply(new Bitmap(this.Image));
            //    //        CannyEdgeDetector edgesFilter = new CannyEdgeDetector();
            //    //        maskBitmap = edgesFilter.Apply(maskBitmap);
            //    //    }
            //    //#endregion

            //    return maskBitmap;
            //}

            get
            {
                if (maskBitmap == null)
                    if (!String.IsNullOrEmpty(mask))
                        maskBitmap = new ResizeNearestNeighbor(100, 100).Apply(new Bitmap(this.Mask));

                return maskBitmap;
            }
        }

        /// <summary>
        /// Zwraca bitmape 100x100 obrazka wzorca
        /// <remarks>
        /// Tworzona tylko raz, na żądanie użytkownika
        /// </remarks>
        /// </summary>
        public Bitmap PatternBitmap
        {
            get
            {
                if (patternBitmap == null)
                    if (!String.IsNullOrEmpty(Image))
                        patternBitmap = new ResizeNearestNeighbor(100, 100).Apply(new Bitmap(this.Image));

                return patternBitmap;
            }
        }

        public Pattern(String id, int treshold, String Name, String Image, String Mask)
        {
            this.Id = id;
            this.Treshold = treshold;
            this.Name = Name;
            this.Image = Image;
            this.Mask = Mask;
        }


    }

    public class PatternDB
    {
        public String DBFile { get; private set; }
        public bool Connected { get; private set; }

        private XDocument DB;

        public bool Connect(string DBFile)
        {
            try
            {
                DB = XDocument.Load(DBFile);
                XmlSchemaSet Schemas = new XmlSchemaSet();
                Schemas.Add(null, @"PatternDBSchema.xsd");
                DB.Validate(Schemas, null, true);
                Connected = true;
                this.DBFile = DBFile;
                return true;
            }
            catch
            {
                Connected = false;
                this.DBFile = "";
                return false;
            }
        }

        public Pattern getById(string id)
        {
            if (!Connected) return null;
            var query = (from pattern in DB.Root.Elements("Pattern")
                         where pattern.Attribute("id").Value == id
                         select new Pattern(id,
                                            (int) pattern.Attribute("treshold"),
                                            (string) pattern.Element("Name"),
                                            (string) pattern.Element("Image"),
                                            (string) pattern.Element("Mask")
                        ));
            return query.Any() ? query.First() : null;
        }

        public Pattern getByName(string Name)
        {
            if (!Connected) return null;
            var query = (from pattern in DB.Root.Elements("Pattern")
                         where pattern.Element("Name").Value == Name
                         select new Pattern((string) pattern.Attribute("id"),
                                            (int) pattern.Attribute("treshold"),
                                            (string) pattern.Element("Name"),
                                            (string) pattern.Element("Image"),
                                            (string)pattern.Element("Mask")
                        ));
            return query.Any() ? query.First() : null;
        }

        public List<Pattern> getAllPatterns()
        {
            if (!Connected) return null;
            IEnumerable<Pattern> query = from pattern in DB.Root.Elements("Pattern")
                                         select new Pattern((string) pattern.Attribute("id"),
                                                            (int) pattern.Attribute("treshold"),
                                                            (string) pattern.Element("Name"),
                                                            (string) pattern.Element("Image"),
                                                            (string)pattern.Element("Mask"));
            
            
            //czy jeszcze nie dodano plikow z resourcow

            if (query.Count()==0)
            {
                //tu bedz
                //Update(new Pattern("1", 10, "Opelek", Environment.CurrentDirectory + "\\Images\\Logos\\opel_logo.jpg",
                          //         Environment.CurrentDirectory + "\\Images\\Masks\\opel_logo.jpg"));


                Update(new Pattern("1", 2048, "Audi", Environment.CurrentDirectory + "\\Images\\Logos\\audi-logo2.jpg",
                                   Environment.CurrentDirectory + "\\Images\\Masks\\audi-logo-mask.bmp"));

                Update(new Pattern("2", 1316, "Ford", Environment.CurrentDirectory + "\\Images\\Logos\\Ford_Logo.jpg",
                   Environment.CurrentDirectory + "\\Images\\Masks\\Ford-maska.bmp"));

                Update(new Pattern("3", 1295, "Peugeot", Environment.CurrentDirectory + "\\Images\\Logos\\s-peugeot.jpg",
    Environment.CurrentDirectory + "\\Images\\Masks\\s-peugeot-maska.bmp"));
                //Update(new Pattern("3", 2000, "Opel", Environment.CurrentDirectory + "\\Images\\Logos\\opel.jpg",
                //   Environment.CurrentDirectory + "\\Images\\Masks\\opel-maska.bmp"));

                //Update(new Pattern("4", 1881, "Toyota", Environment.CurrentDirectory + "\\Images\\Logos\\toyota_logo.jpg",
                //    Environment.CurrentDirectory + "\\Images\\Masks\\toyota-logo-mask-small.bmp"));

                //Update(new Pattern("5", 800, "Volvo", Environment.CurrentDirectory + "\\Images\\Logos\\volvo-2006-logo1.jpg",
                //    Environment.CurrentDirectory + "\\Images\\Masks\\volvo-prosty.bmp"));
                
                //kolejne updaty

                //ponowne wczytanie

                query = from pattern in DB.Root.Elements("Pattern")
                        select new Pattern((string)pattern.Attribute("id"),
                                           (int)pattern.Attribute("treshold"),
                                           (string)pattern.Element("Name"),
                                           (string)pattern.Element("Image"),
                                           (string)pattern.Element("Mask"));
            }


            List<Pattern> lista = new List<Pattern>(query.Count());
            foreach (Pattern pattern in query)
                lista.Add(pattern);

            return lista;
        }

        public String GetNextId()
        {
            if (!Connected) return null;
            var query = from pattern in DB.Root.Elements("Pattern")
                        select (int)pattern.Attribute("id");
            return query.Any() ? (query.Max()+1).ToString() : "1";
        }

        public int Count()
        {
            if (!Connected) return 0;
            return DB.Root.Elements("Pattern").Count();
        }

        public bool Update(Pattern pattern)
        {
            if (!Connected) return false;
            try
            {
                IEnumerable<XElement> Query = from pat in DB.Root.Elements("Pattern")
                                              where (string)pat.Attribute("id") == pattern.Id
                                              select pat;
                if (Query.Any())
                {
                    XElement Element = Query.First();
                    Element.Attribute("treshold").SetValue(pattern.Treshold);
                    Element.Element("Name").SetValue(pattern.Name);
                    Element.Element("Image").SetValue(pattern.Image);
                    Element.Element("Mask").SetValue(pattern.Mask);
                }
                else
                {
                    DB.Root.Add(new XElement("Pattern",
                                             new XAttribute("id", pattern.Id),
                                             new XAttribute("treshold", pattern.Treshold),
                                             new XElement("Name", pattern.Name),
                                             new XElement("Image", pattern.Image),
                                             new XElement("Mask", pattern.Mask)));
                }

                DB.Save(DBFile);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool Remove(Pattern pattern)
        {
            if (!Connected) return false;
            IEnumerable<XElement> Query = from pat in DB.Root.Elements("Pattern")
                                          where (string)pat.Attribute("id") == pattern.Id
                                          select pat;
            if (Query.Any())
            {
                Query.First().Remove();
                DB.Save(DBFile);
                return true;
            }
            return false;
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            if (!Connected) yield break;
            IEnumerable<Pattern> query = from pattern in DB.Root.Elements("Pattern")
                                         select new Pattern((string)pattern.Attribute("id"),
                                                            (int)pattern.Attribute("treshold"),
                                                            (string)pattern.Element("Name"),
                                                            (string)pattern.Element("Image"),
                                                            (string)pattern.Element("Mask"));
            foreach (Pattern pattern in query)
                yield return pattern;
        }
    }
}
